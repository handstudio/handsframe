var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();

/**
 * @public
 * @description AVPlayer 초기화
 */
function AVPlayerInit() {
	// AVPlayer 초기화 : webapis 플러그인 사용
	var playerInstance = webapis.avplay;
	playerInstance.getAVPlay(onAVPlayObtained, onGetAVPlayError);

	//onAVPlayObtained 함수(콜백함수) : 실제적인 AVPlayer 모듈을 불러오는 역할을 함.
	function onAVPlayObtained(avplay) {
		Main.AVPlayer = avplay;
		Main.AVPlayer.init();
	}
	//AVPlayer 모듈을 초기화 할때 발생하는 에러를 처리하기 위한 함수.
	function onGetAVPlayError(error) {
		alert('#####onGetAVPlayError: ' + error.message);
	}
};

/**
 * @public
 * @description AVPlayer 재생
 */
function AVPlayerRun() {
	//재생할 미디어 콘텐츠 설정.
	Main.AVPlayer.open('http://www.w3schools.com/tags/movie.mp4', {});
	//콘텐츠 재생.
	Main.AVPlayer.play(onSuccessCB, onErrorCB);

	// Player Success Callback
	function onSuccessCB() {
		alert('Play Success');
	}
	// Player Error Callback
	function onErrorCB(error) {
		alert('Play Error' + error.message);
	}
}

var Main = {};
Main.onLoad = function() {
	// Enable key event processing
	this.enableKeys();
	widgetAPI.sendReadyEvent();

	AVPlayerInit();
	AVPlayerRun();
};

Main.onUnload = function() {};

Main.enableKeys = function() {
	document.getElementById('anchor').focus();
};

Main.keyDown = function() {
	var keyCode = event.keyCode;

	switch(keyCode) {
		case tvKey.KEY_RETURN:
			break;
		case tvKey.KEY_PLAY:
			//플레이어 다시 재생
			Main.AVPlayer.resume();
			break;
		case tvKey.KEY_STOP:
			//플레이어 정지.
			Main.AVPlayer.stop();
			break;
		case tvKey.KEY_PAUSE:
			//플레이어 일시정지
			Main.AVPlayer.pause();
			break;
		case tvKey.KEY_RW:
			//현재 클립에서 5초 뒤로 건너 뛰기
			Main.AVPlayer.jumpBackward(5);
			break;
		case tvKey.KEY_FF:
			//현재 클립에서 5초 앞으로 건너 뛰기
			Main.AVPlayer.jumpForward(5);
			break;
	}
};