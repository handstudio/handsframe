package com.handstudio.android.convergence;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

public class SimpleNavigatorActivity extends Activity {
	public static final String TYPE_INPUT		= "TYPE_INPUT";
	public static final String TYPE_CONVERGENCE = "TYPE_CONVERGENCE";
	
	//TV로 보낼 메시지.
	public static final String MSG_LEFT 	= "left";
	public static final String MSG_RIGHT 	= "right";
	public static final String MSG_UP 		= "up";
	public static final String MSG_DOWN 	= "down";
	public static final String MSG_OKAY 	= "enter";
	public static final String MSG_RETURN 	= "return";
	
	//버튼 View.
	private Button mBtnConnect;
	private Button mBtnDisconnect;
	
	private ImageButton mBtnLeft;
	private ImageButton mBtnRight;
	private ImageButton mBtnUp;
	private ImageButton mBtnDown;
	private ImageButton mBtnOkay;
	private ImageButton mBtnReturn;
	
	private ScrollView mScrollview;
	private TextView mTxtLog;
	
	//통신 관련 멤버 변수들. 
	private ConvergenceUtil mConvergenceUtil;
	private IntentFilter mIntentFilter;
	private ConnectThread mConnectThread;
	private ReceiveThread mReceiveThread;
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if (android.os.Build.VERSION.SDK_INT > 10) 	// ICS Version 호환을 위함.
		{
	      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	      StrictMode.setThreadPolicy(policy);
		}
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // data 를 받기 위한 intent filter 설정 
 		mIntentFilter = new IntentFilter(); 
 		mIntentFilter.addAction(ConvergenceUtil.RECEIVE);
 		
 		// 리시버 등록 
 		if(null!=mReceiver)
 		{
 			registerReceiver(mReceiver, mIntentFilter);	
 		}	
 		
 		this.initLayout();
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		stopReceiveThread();
		mConvergenceUtil.disconnect();
		
		if(null!=mReceiver)
		{
			unregisterReceiver(mReceiver);	
		}	
	}
	
	private void initLayout()
	{
		mBtnConnect 	= (Button) findViewById(R.id.btnConnect);
		mBtnDisconnect 	= (Button) findViewById(R.id.btnDisconnect);
		
		mBtnConnect.setOnClickListener(clickListener);
		mBtnDisconnect.setOnClickListener(clickListener);
		
		mBtnLeft 	= (ImageButton) findViewById(R.id.btnLeft);
		mBtnRight 	= (ImageButton) findViewById(R.id.btnRight);
		mBtnUp 		= (ImageButton) findViewById(R.id.btnUp);
		mBtnDown 	= (ImageButton) findViewById(R.id.btnDown);
		mBtnOkay 	= (ImageButton) findViewById(R.id.btnOkay);
		mBtnReturn 	= (ImageButton) findViewById(R.id.btnReturn);
		
		mBtnLeft.setOnClickListener(clickListener);
		mBtnRight.setOnClickListener(clickListener);
		mBtnUp.setOnClickListener(clickListener);
		mBtnDown.setOnClickListener(clickListener);
		mBtnOkay.setOnClickListener(clickListener);
		mBtnReturn.setOnClickListener(clickListener);
		
		mScrollview = (ScrollView) findViewById(R.id.scrollview);
		mTxtLog = (TextView) findViewById(R.id.txtLog);
		
		mConvergenceUtil = new ConvergenceUtil(getApplicationContext());
	}
	
	/**
	 * 모바일에서 버튼 선택 시 수행
	 * @param view 선택된 버튼 view 
	 */
	OnClickListener clickListener = new OnClickListener() {
		
		public void onClick(View view) {
			if(view.equals(mBtnLeft)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_LEFT);	
			}
			else if(view.equals(mBtnRight)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_RIGHT);	
			}
			else if(view.equals(mBtnUp)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_UP);	
			}
			else if(view.equals(mBtnDown)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_DOWN);	
			}
			else if(view.equals(mBtnOkay)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_OKAY);	
			}
			else if(view.equals(mBtnReturn)){
				mConvergenceUtil.sendMessage(TYPE_INPUT, MSG_RETURN);	
			}
			else if(view.equals(mBtnConnect)){
				startConnectThread();
				startReceiveThread();
			}
			else if(view.equals(mBtnDisconnect)){
				mConvergenceUtil.disconnect();
				stopReceiveThread();
			}
		}
	};
	
	/**
	 * TV로부터 전달받은 메시지에 따라 수행 
	 */
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ConvergenceUtil.RECEIVE)) {
				String type = intent.getStringExtra("type");
				String msg = intent.getStringExtra("msg");
				
				if(type!=null && msg != null){
					
					if(type.equals(TYPE_CONVERGENCE)){
						mTxtLog.append(""+msg+"\n");
					}else if(type.equals(TYPE_INPUT)){
						mTxtLog.append("[ReceivedMessage] "+msg+"\n");
					}					
					
					mScrollview.post(new Runnable() { 
				        public void run() { 
				        	mScrollview.scrollTo(0, mScrollview.getBottom());
				        } 
					});
				}
			}
		}
	};

	/**
	 * TV와의 연결을 시도하기 위한 thread 
	 */
	private class ConnectThread extends Thread {
		boolean m_runFlag = true;
		
		public void setRunFlag(boolean bFlag)
		{
			m_runFlag = bFlag;
		}
		
		@Override
		public void run() {
			while (m_runFlag) {
				synchronized (getApplicationContext()) {					
					mConvergenceUtil.connect();
					stopConnectThread();
				}
			}			
			super.run();			
		}
	}
	
	private void startConnectThread()
	{
		stopConnectThread();
		mConnectThread	= new  ConnectThread();
		
		if(null!=mConnectThread)
		{
			mConnectThread.start();
		}
	}
	
	private void stopConnectThread()
	{
		if(null!=mConnectThread)
		{
			Thread tmpThread = mConnectThread;
			mConnectThread.setRunFlag(false);
			mConnectThread = null;
			tmpThread.interrupt();
		}
	}
	
	/**
	 * TV로 부터의 메시지를 수신하기 위한 thread
	 */	
	private class ReceiveThread extends Thread
	{	
		boolean m_runFlag = true;
		
		public void setRunFlag(boolean bFlag)
		{
			m_runFlag = bFlag;
		}
		
		public void run()
		{
			while(m_runFlag)
			{
				mConvergenceUtil.receiveMessage();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void startReceiveThread()
	{
		stopReceiveThread();
		mReceiveThread = new ReceiveThread();
        mReceiveThread.start();
	}
	
	private void stopReceiveThread()
	{
		if(null!=mReceiveThread)
		{
			Thread tmpThread = mReceiveThread;
			mReceiveThread.setRunFlag(false);
			mReceiveThread = null;
			tmpThread.interrupt();
		}
	}
}
