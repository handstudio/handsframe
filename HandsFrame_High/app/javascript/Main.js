var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();

var Main = {
	category : {
		elem : jQuery('#category'),
		li	 : jQuery('#category').find('ul > li'),
		anchor : jQuery('#anchor_category'),
	},
	content : {
		elem : jQuery('#content'),
		div : jQuery('#content > div'),
		anchor : jQuery('#anchor_content')
	},
	player : {
		anchor : jQuery('#anchor_player')
	},
	info : {
		elem : jQuery('#scene_info'),
		anchor : jQuery('#anchor_info')
	},
	login : {
		elem : jQuery('#header > .login'),
		form : jQuery('#login_form')
	}
};

//IME 오브젝트 선언
var oIME = null;

//로그인이 됐는지 안됐는지를 나타내는 논리값
var login_flag = false;


var index = 0;
var content_index = 0;
var focused_comp = 'category'; //INFO Scene을 위해서 현재 포커스된 요소를 저장하는 변수


//카테고리 인덱스와 콘텐트 인덱스에 따라 분류된 URL 2차 배열
var url = [[getAbsPath('resource/1_1.mp4'), getAbsPath('resource/1_2.mp4')],
           [getAbsPath('resource/2_1.mp4'), getAbsPath('resource/2_2.mp4')],
           [getAbsPath('resource/3_1.mp4'), getAbsPath('resource/3_2.mp4')]];

var Player = {
		init : function(){
			try{
				var playerInstance = webapis.avplay;
				playerInstance.getAVPlay(Player.onAVPlayObtained, Player.onGetAVPlayError);
			
			}catch(e){
				alert('######getAVplay Exception :[' +e.code + '] ' + e.message);
			}
		},
		onAVPlayObtained : function(avplay){	
			//AVPlayer 모듈을 초기화 콜백함수
			Main.AVPlayer = avplay;
			Main.AVPlayer.init({zIndex : 2, containerID : 'player_container', displayRect: {
				top: 0,
				left: 0,
				width: 1280,
				height: 720
			}, autoRatio: true });
		},
		onGetAVPlayError : function(){ 
			//AVPlayer 모듈 초기화시 발생한 에러 핸들링함수
			alert('######onGetAVPlayError: ' + error.message);
		},
		onError : function(){
			alert('######onError: ');
		},
		onSuccess : function(){
			alert('######onSuccess: ');
		},
		play: function() {
			try{
				alert('url[index][content_index] : !!!!!!!!!!'+url[index][content_index]);
				jQuery('#player_container').addClass('show');
				Main.AVPlayer.open(url[index][content_index]);
				Main.AVPlayer.play(Player.onSuccess, Player.onError);
				
			}catch(e){
				alert(e.message);
			}
		},
		stop: function() {
			jQuery('#player_container').removeClass('show');
			Main.AVPlayer.stop();
		}
};

Main.onLoad = function()
{
	 
	this.focus();	// 초기 포커스 설정
	widgetAPI.sendReadyEvent();
	Main.loadContent();	//현재 카테고리에 해당하는 View 콘텐츠 로드
	Player.init(); //Player 모듈 초기화
};

Main.loadContent = function(){
	jQuery('#content').find('div').hide();
	jQuery('#content').find('div').eq(index).show();
};

//애플리케이션의 종료시점에 호출되는 이벤트 처리 함수
Main.onUnload = function()
{
	if(oIME){
		oIME.onClose();
	}
};
Main.enableKeys = function()	
{
	
};

Main.focus = function(){ 
	Main.category.anchor.focus();
	Main.category.elem.addClass('focus');
	Main.category.li.eq(index).addClass('focus');
	
};

Main.category.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			widgetAPI.sendReturnEvent();
			break;
		case tvKey.KEY_LEFT:
			break;
		case tvKey.KEY_RIGHT:
			Main.content.anchor.focus();
			Main.category.elem.removeClass('focus');
			Main.content.elem.addClass('focus');
			Main.content.div.eq(index).find('li').eq(content_index).addClass('focus');
			focused_comp = 'content';
			break;
		case tvKey.KEY_UP:
			if(index == 0){
				if(!login_flag){
					Main.login.form.focus();
					Main.category.elem.removeClass('focus');
					Main.login.elem.addClass('focus');
				}
			}else{
				Main.category.li.eq(index).removeClass('focus');
				Main.category.li.eq(--index).addClass('focus');
				Main.loadContent();
			}
			break;
		case tvKey.KEY_DOWN:
			if(index < Main.category.li.size() - 1){
				Main.category.li.eq(index).removeClass('focus');
				Main.category.li.eq(++index).addClass('focus');
				
				Main.loadContent();
			}
			break;
		case tvKey.KEY_INFO :
			Main.info.elem.show();
			Main.info.anchor.focus();
			break;
		default:
			alert("Unhandled key");
			break;
	}
};

Main.content.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			widgetAPI.sendReturnEvent();
			break;
		case tvKey.KEY_LEFT:
			if(content_index == 1){
				Main.content.div.eq(index).find('li').eq(content_index).removeClass('focus');
				Main.content.div.eq(index).find('li').eq(--content_index).addClass('focus');
			}else{
				Main.category.anchor.focus();
				Main.content.elem.removeClass('focus');
				Main.category.elem.addClass('focus');
				focused_comp = 'category';
			}
			break;
		case tvKey.KEY_RIGHT:
			if(content_index == 0){
				Main.content.div.eq(index).find('li').eq(content_index).removeClass('focus');
				Main.content.div.eq(index).find('li').eq(++content_index).addClass('focus');
			}else{
				return false;
			}
			break;
		case tvKey.KEY_ENTER:
			Player.play();
			Main.player.anchor.focus();
			break;
		case tvKey.KEY_INFO :
			Main.info.elem.show();
			Main.info.anchor.focus();
			break;
		default:
			break;
	}
};

Main.player.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			event.preventDefault();
			Player.stop();
			Main.content.anchor.focus();
			break;
		case tvKey.KEY_PLAY:
			Player.play();
			break;
		case tvKey.KEY_STOP:
			Player.stop();
			Main.content.anchor.focus();
			break;
		default:
			break;
	}
};

Main.info.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			event.preventDefault();
			Main.info.elem.hide();
			if(focused_comp=='category'){
				Main.category.anchor.focus();
			}else if(focused_comp=='content'){
				Main.content.anchor.focus();	
			}
			break;
		default:
			break;
	}
};
Main.login.keyDown = function(){
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			event.preventDefault();
			widgetAPI.sendReturnEvent();
		break;
		case tvKey.KEY_DOWN:
			Main.category.anchor.focus();
			Main.login.elem.removeClass('focus');
			Main.category.elem.addClass('focus');
			break;
		case tvKey.KEY_ENTER:
			focusIME();
			break;
		default:
			break;
	}
};


var focusIME = function() {
	oIME = new IMEShell_Common(); 
    oIME.inputboxID = "login_form";
    oIME.onKeyPressFunc = function(nKeyCode) {
    	switch(nKeyCode)
    	{
    		case tvKey.KEY_RETURN:
    	    	break;
    		case tvKey.KEY_EXIT:
    			break;
    		case tvKey.KEY_ENTER:
    			form_submit();
    			return false;
    			break;
    	}
    };
    
    Main.login.form.focus();
	oIME.onShow();
};

var form_submit = function(){
	Main.login.elem.empty();
	Main.login.elem.text('WelCome ! '+Main.login.form.val() + '.');
	
	login_flag = true;	//로그인이 됐는지 안됐는지를 나타내는 논리값
	
	Main.category.anchor.focus();
	Main.login.elem.removeClass('focus');
	Main.category.elem.addClass('focus');
};

var Convergence = {
    api: window.webapis.customdevice || {},
    aDevice: [],
    init: function() {
        this.api.registerManagerCallback(Convergence.registerManager);
        this.api.getCustomDevices(Convergence.getCustomDevices);
    },
    registerManager: function(oManagerEvent) {
        var _this = Convergence;	        
        switch(oManagerEvent.eventType) {
            case _this.api.MGR_EVENT_DEV_CONNECT:
                _this.api.getCustomDevices(Convergence.getCustomDevices);
                break;
            case _this.api.MGR_EVENT_DEV_DISCONNECT: 
                _this.api.getCustomDevices(Convergence.getCustomDevices);
                break;
            default: 
                break;
        }
    },
    getCustomDevices: function(aDevice) {
        var _this = Convergence;
        _this.aDevice = aDevice;
        
        for(var i = 0; i < aDevice.length; i++) {
            var sID = aDevice[i].getUniqueID();	            
            aDevice[i].registerDeviceCallback(function(oDeviceInfo) {
                _this.registerDevice(sID, oDeviceInfo);
            });
        }
    },
    registerDevice: function(sID, oDeviceInfo) {
        var mobileKeyEvent = jQuery.parseJSON(oDeviceInfo.data.message1);
        handleMobileEvent(mobileKeyEvent.msg);
    },
    sendMessage: function(oDevice, sMessage) {
        return oDevice.sendMessage(sMessage);
    },
    broadcastMessage: function(sMessage) {
        return this.aDevice[0] && this.aDevice[0].broadcastMessage(sMessage);
    },
    uploadFile: function(sName) {
        //sName: 이미지 파일 이름
        var sUrl = 'http://127.0.0.1/ws/app/' + curWidget.id  + '/file/' + sName;
        return '<img src="' + sUrl + '"/>';
    }
};
Convergence.init();


//요청한 이벤트를 핸들링하는 함수
var handleMobileEvent = function(event){
	switch(event) {
		case 'msg_show' :
			$('#convergence_help').show();
			break;
		case 'msg_hide' :
			$('#convergence_help').hide();
			break;
	}
};

