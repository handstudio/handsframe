package com.hz.allshareplayer;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.sec.android.allshare.Device;
import com.sec.android.allshare.Device.DeviceType;
import com.sec.android.allshare.DeviceFinder;
import com.sec.android.allshare.DeviceFinder.IDeviceFinderEventListener;
import com.sec.android.allshare.ERROR;
import com.sec.android.allshare.Item;
import com.sec.android.allshare.ServiceConnector;
import com.sec.android.allshare.ServiceConnector.IServiceConnectEventListener;
import com.sec.android.allshare.ServiceConnector.ServiceState;
import com.sec.android.allshare.ServiceProvider;
import com.sec.android.allshare.media.AVPlayer;
import com.sec.android.allshare.media.AVPlayer.AVPlayerState;
import com.sec.android.allshare.media.ContentInfo;
import com.sec.android.allshare.media.MediaInfo;

public class AllSharePlayerActivity extends Activity {
	
	public static final String TAG = "AllSharePlayerActivity";
	private ServiceProvider mServiceProvider = null;
	private TextView mText = null;
	
	private String mDeviceID = null;	
	private AVPlayer mPlayer = null;
	private Item mItem = null;
	private String filePath = "/mnt/sdcard/Movies/test.mp4";
	private String mimeType = "video/mp4";
	boolean isPlay = false;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mText = (TextView) findViewById(R.id.txtLog);
		mText.append("\n\n" + "Creating service provider!"  + "\r\n\n");

		ServiceConnector.createServiceProvider(this, new IServiceConnectEventListener()
		{
			@Override
			public void onCreated(ServiceProvider sprovider, ServiceState state)
			{
				mServiceProvider = sprovider;
				showDeviceList();

			}
			@Override
			public void onDeleted(ServiceProvider sprovider)
			{
				mServiceProvider = null;
			}
		});
	}
    
	private final DeviceFinder.IDeviceFinderEventListener mDeviceDiscoveryListener = new IDeviceFinderEventListener()
	{
		@Override
		public void onDeviceRemoved(DeviceType deviceType, Device device, ERROR err)
		{
			mText.append("AVPlayer: " + device.getName() + " [" + "] is removed" + "\r\n");
		}

		@Override
		public void onDeviceAdded(DeviceType deviceType, Device device, ERROR err)
		{
			mText.append("Add - AVPlayer: " + device.getName() + " [" + "] is found" + "\r\n");
			if(!isPlay){
				mText.append("  Play : " + device.getName() + " [" + "]" + "\r\n");
				startAllShare(device.getID());
			}
		}
	};
	
	private void showDeviceList()
	{
		if (mServiceProvider == null)
			return;

		DeviceFinder mDeviceFinder = mServiceProvider.getDeviceFinder();
		mDeviceFinder.setDeviceFinderEventListener(DeviceType.DEVICE_AVPLAYER, mDeviceDiscoveryListener);
		ArrayList<Device> mDeviceList = mDeviceFinder.getDevices(DeviceType.DEVICE_AVPLAYER);
		
		if (mDeviceList != null)
		{
			for (int i = 0; i < mDeviceList.size(); i++)
			{
				mText.append("AVPlayer: " + mDeviceList.get(i).getName() + " [" + "] is found" + "\r\n");
				if(!isPlay){
					mText.append("  Play : " + mDeviceList.get(i).getName() + " [" + "]" + "\r\n");
					startAllShare(mDeviceList.get(i).getID());
				}
			}
		}
	}


	@Override
	protected void onDestroy()
	{
		if (mServiceProvider != null)
			ServiceConnector.deleteServiceProvider(mServiceProvider);
		super.onDestroy();
	}
	
	private void initItem() {
		
		DeviceFinder deviceFinder = mServiceProvider.getDeviceFinder();
		AVPlayer avPlayer = (AVPlayer) deviceFinder.getDevice(mDeviceID, DeviceType.DEVICE_AVPLAYER);

		if (avPlayer == null)
		{
			return;
		}

		Item.LocalContentBuilder lcb = new Item.LocalContentBuilder(filePath, mimeType);
		lcb.setTitle("");
		mItem = lcb.build();
		mPlayer = avPlayer;
		
		mItem = new Item.LocalContentBuilder(filePath, mimeType).build();
		
	}
	
	private void startAllShare(String deviceId) {
		mDeviceID = deviceId;
		initItem();
		registerEventListener();
		registerResponseHandler();
		play();
		isPlay = true;
	}
	
	private void play() {
		ContentInfo.Builder builder = new ContentInfo.Builder();
		ContentInfo info = builder.build();
		mPlayer.play(mItem, info);
	}
	
	/**
	 * register AVPlayer state changed callback function
	 */
	private void registerEventListener()
	{

		if (mPlayer == null)
			return;

		mPlayer.setEventListener(new AVPlayer.IAVPlayerEventListener()
		{

			@Override
			public void onDeviceChanged(AVPlayerState state, ERROR err)
			{
				Log.d(TAG,"onDeviceChanged  state = " + state);
				if (AVPlayerState.PLAYING.equals(state))
				{
					mPlayer.getMediaInfo();
				}
			}
		});
	}
	
	/**
	 * register AVPlayer callback function
	 */
	private void registerResponseHandler()
	{

		if (mPlayer == null)
			return;

		mPlayer.setResponseListener(new AVPlayer.IAVPlayerPlaybackResponseListener()
		{
			@Override
			public void onStopResponseReceived(ERROR err){
			}

			@Override
			public void onGetMediaInfoResponseReceived(MediaInfo arg0,
					ERROR arg1) {
			}

			@Override
			public void onGetPlayPositionResponseReceived(long arg0, ERROR arg1) {
			}

			@Override
			public void onGetStateResponseReceived(AVPlayerState arg0,
					ERROR arg1) {
			}

			@Override
			public void onPauseResponseReceived(ERROR arg0) {
			}

			@Override
			public void onPlayResponseReceived(Item arg0, ContentInfo arg1,
					ERROR arg2) {
			}

			@Override
			public void onResumeResponseReceived(ERROR arg0) {
			}

			@Override
			public void onSeekResponseReceived(long arg0, ERROR arg1) {
			}
		});
	}
    
}