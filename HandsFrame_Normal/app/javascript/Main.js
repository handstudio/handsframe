var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();

var Main = {
	category : {
		elem : jQuery('#category'),
		li	 : jQuery('#category').find('ul > li'),
		anchor : jQuery('#anchor_category')
	},

	content : {	
		elem : jQuery('#content'),
		div : jQuery('#content > div'),
		anchor : jQuery('#anchor_content')
	},
	//추가된 플레이어 앵커 요소
	player : {
		anchor : jQuery('#anchor_player')
	},
	info : {
		elem : jQuery('#scene_info'),
		anchor : jQuery('#anchor_info')
	}
};


var index = 0;
var content_index = 0;

//INFO Scene을 위해서 현재 포커스된 요소를 저장하는 변수
var focused_comp = 'category'; 


//카테고리 인덱스와 콘텐트 인덱스에 따라 분류된 URL 2차 배열
var url = [[getAbsPath('resource/1_1.mp4'), getAbsPath('resource/1_2.mp4')],
           [getAbsPath('resource/2_1.mp4'), getAbsPath('resource/2_2.mp4')],
           [getAbsPath('resource/3_1.mp4'), getAbsPath('resource/3_2.mp4')]];


var Player = {
	init : function(){
		try{
			var playerInstance = webapis.avplay;
			playerInstance.getAVPlay(Player.onAVPlayObtained, Player.onGetAVPlayError);
		
		}catch(e){
			alert('######getAVplay Exception :[' +e.code + '] ' + e.message);
		}
	},
	onAVPlayObtained : function(avplay){	
		//AVPlayer 모듈을 초기화 콜백함수
		Main.AVPlayer = avplay;
		Main.AVPlayer.init({containerID : 'player_container', displayRect: {
			top: 0,
			left: 0,
			width: 1280,
			height: 720
		}, autoRatio: true });
	},
	onGetAVPlayError : function(){ 
		//AVPlayer 모듈 초기화시 발생한 에러 핸들링함수
		alert('######onGetAVPlayError: ' + error.message);
	},
	onError : function(){
		alert('######onError: ');
	},
	onSuccess : function(){
		alert('######onSuccess: ');
	},
	play: function() {
		try{
			jQuery('#player_container').addClass('show');
			Main.AVPlayer.open(url[index][content_index]);
			Main.AVPlayer.play(Player.onSuccess, Player.onError);
			
		}catch(e){
			alert(e.message);
		}
	},
	stop: function() {
		jQuery('#player_container').removeClass('show');
		Main.AVPlayer.stop();
	}
};


Main.onLoad = function()
{
	this.focus();	// 초기 포커스 설정
	widgetAPI.sendReadyEvent();
	Main.loadContent();	//현재 카테고리에 해당하는 View 콘텐츠 로드
	Player.init();
};

//카테고리 이동에 따라 콘텐트 영역 불러오기
Main.loadContent = function(){
	Main.content.div.hide();
	Main.content.div.eq(index).show();
};


Main.onUnload = function()
{

};
Main.enableKeys = function()	
{
	
};

//애플리케이션 초기 포커스
Main.focus = function(){ 
	Main.category.anchor.focus();
	Main.category.elem.addClass('focus');
	Main.category.li.eq(index).addClass('focus');
};

Main.category.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			widgetAPI.sendReturnEvent();
			break;
		case tvKey.KEY_RIGHT:
			Main.content.anchor.focus();
			Main.category.elem.removeClass('focus');
			Main.content.elem.addClass('focus');
			Main.content.div.eq(index).find('li').eq(content_index).addClass('focus');
			focused_comp = 'content';
			break;
		case tvKey.KEY_UP:
			if(index > 0){
				Main.category.li.eq(index).removeClass('focus');
				Main.category.li.eq(--index).addClass('focus');
				Main.loadContent();
			}
			break;
		case tvKey.KEY_DOWN:
			if(index < Main.category.li.size() - 1){
				Main.category.li.eq(index).removeClass('focus');
				Main.category.li.eq(++index).addClass('focus');
				Main.loadContent();
			}
			break;
		case tvKey.KEY_INFO :
			Main.info.elem.show();
			Main.info.anchor.focus();
			break;
		default:
			alert("Unhandled key");
			break;
	}
};

Main.content.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			widgetAPI.sendReturnEvent();
			break;
		case tvKey.KEY_LEFT:
			if(content_index == 1){
				Main.content.div.eq(index).find('li').eq(content_index).removeClass('focus');
				Main.content.div.eq(index).find('li').eq(--content_index).addClass('focus');
			}else{
				Main.category.anchor.focus();
				Main.content.elem.removeClass('focus');
				Main.category.elem.addClass('focus');
				focused_comp = 'category';
			}
			break;
		case tvKey.KEY_RIGHT:
			if(content_index == 0){
				Main.content.div.eq(index).find('li').eq(content_index).removeClass('focus');
				Main.content.div.eq(index).find('li').eq(++content_index).addClass('focus');
			}else{
				return false;
			}
			break;
		case tvKey.KEY_ENTER:
			Player.play();
			Main.player.anchor.focus();
			break;
		case tvKey.KEY_INFO :
			Main.info.elem.show();
			Main.info.anchor.focus();
			break;
		default:
			break;
	}
};

Main.player.keyDown = function()
{
	var keyCode = event.keyCode;
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			event.preventDefault();
			Player.stop();
			Main.content.anchor.focus();
			break;
		case tvKey.KEY_PLAY:
			Player.play();
			break;
		case tvKey.KEY_STOP:
			Player.stop();
			Main.content.anchor.focus();
			break;
		default:
			break;
	}
};

Main.info.keyDown = function()
{
	var keyCode = event.keyCode;
	
	switch(keyCode)
	{
		case tvKey.KEY_RETURN:
			event.preventDefault();
			Main.info.elem.hide();
			if(focused_comp=='category'){
				Main.category.anchor.focus();
			}else if(focused_comp=='content'){
				Main.content.anchor.focus();	
			}
			break;
		default:
			break;
	}
};
